<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="modelo.Cliente"
	import="controlador.Controlador" import="java.util.List"
	import="modelo.Alquiler" import="modelo.Pago" import="modelo.Casa"
	import="modelo.Departamento" import="modelo.LocalComercial"
	import="modelo.Terreno" import="modelo.Contrato" import="modelo.Venta"
	import="java.time.LocalDate"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Cuenta corriente</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>


	<%
	Cliente cliente = (Cliente) request.getSession().getAttribute("cliente");
	Controlador controlador = new Controlador();
	List<Pago> pagos = null;
	List<Alquiler> alquileres = null;
	List<Venta> ventas = null;
	String ubicacion = "";

	if (cliente != null) {

		alquileres = controlador.getAlquileresPorDocumento(cliente.getDocumento());
		ventas = controlador.getVentasPorDocumento(cliente.getDocumento());
		if (request.getParameter("contrato") != null) {
			Contrato contrato = new Contrato();
			contrato.setCod_contrato(Integer.parseInt(request.getParameter("contrato")));
			pagos = controlador.getPagosContrato(contrato);
		}
		
	%>
	<div class="row">
		<div class="col-md-3"></div>

		<div class="col-md-6 text-center mt-5 mb-3">


			<h1>
				�Hola
				<%=cliente.getNombre() + " " + cliente.getApellido()%>
				!
			</h1>
			<h6>
				<a href="<%=request.getContextPath()%>">Salir</a>
			</h6>
			<h6>Esta es tu cuenta corriente</h6>

		</div>
		<div class="col-md-3"></div>
	</div>

	<div class="row ">
		<div class="col-md-3"></div>

		<div class="col-md-6 text-center mb-3">
			<hr>

			<h3>Contratos</h3>


		</div>
		<div class="col-md-3"></div>
	</div>



	<div class="row">
		<div class="col-md-3"></div>

		<div class="col-md-6">
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th>Tipo</th>
						<th>Fecha</th>
						<th>Duraci�n</th>
						<th>Direcci�n</th>
						<th>Acci�n</th>

					</tr>
				</thead>

				<tbody>
					<%
					if(alquileres!=null){
					
					for (Alquiler alquiler : alquileres) {
					%>
					<tr>
					<td>Alquiler</td>
						<td><%=alquiler.getFechaRealizado()%></td>
						<td><%=alquiler.getDuracion()%></td>
						<td>
							<%
							if (alquiler.getCasaAlquilada() != null) {
								ubicacion = alquiler.getCasaAlquilada().getUbicacion();
							}
							if (alquiler.getDepartamentoAlquilado() != null) {
								ubicacion = alquiler.getDepartamentoAlquilado().getUbicacion();
							}
							if (alquiler.getLocalComercialAlquilado() != null) {
								ubicacion = alquiler.getLocalComercialAlquilado().getUbicacion();
							}
							%> <%=ubicacion%> <%
 ubicacion = "";
 %>
						</td>

						<td><a
							href="<%=request.getContextPath()%>/cuentaCorriente.jsp?contrato=<%=alquiler.getCod_contrato()%>">Ver
								Cuotas</a></td>
					</tr>
					<%
					}}
					if(ventas!=null){
					for (Venta venta : ventas) {
					%>
					<tr>
					<td>Compra</td>
						<td><%=venta.getFechaRealizado()%></td>
						<td><%=venta.getDuracion()%></td>
						<td>
							<%
							if (venta.getCasaVendida() != null) {
								ubicacion = venta.getCasaVendida().getUbicacion();
							}
							if (venta.getTerrenoVendido() != null) {
								ubicacion = venta.getTerrenoVendido().getUbicacion();
							}
							%> <%=ubicacion%> <% ubicacion = ""; %>
						</td>

						<td><a
							href="<%=request.getContextPath()%>/cuentaCorriente.jsp?contrato=<%=venta.getCod_contrato()%>">Ver
								Cuotas</a></td>
					</tr>
					<%
					}}
					%>
				</tbody>
			</table>
		</div>
		<div class="col-md-3"></div>
	</div>

	<div class="row">
		<div class="col-md-3"></div>

		<div class="col-md-6 text-center mb-3">

			<hr>
			<h3>Pagos</h3>


		</div>
		<div class="col-md-3"></div>
	</div>
	<%
	if (pagos != null) {
	%>
	<div class="row">
		<div class="col-md-2"></div>

		<div class="col-md-8">
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th>Cuota</th>
						<th>Monto</th>
						<th>1� Vencimiento</th>
						<th>2� Vencimiento</th>
						<th>Estado</th>
						<th>Fecha del Pago</th>
						<th>Acci�n</th>

					</tr>
				</thead>

				<tbody>
					<%
					int i = 1;
					for (Pago pago : pagos) {
					%>
					<tr>
						<td><%="" + i%></td>
						<td><%=pago.getMonto() + ""%></td>
						<td><%=pago.getPrimerVencimiento() + ""%></td>
						<td><%=pago.getSegundoVencimiento() + ""%></td>
						<td>
							<%
							if (pago.getFechaDelPago() == null) {
								if (pago.getPrimerVencimiento().compareTo(LocalDate.now()) < 0) {
							%><%="Impago Vencida"%> <%
							} else {
							%><%="Impago"%> <%
							}
							} else {
							%><%="Pagado"%> <%
							}
							%>
						</td>
						<td>
							<%
							if (pago.getFechaDelPago() == null) {
							%><%="--/--/----"%> <%
							} else {
							%><%=pago.getFechaDelPago()%> <%
							}
							%>
						</td>
						<td><a href="#">Imprimir</a></td>
					</tr>
					<%
					i++;
					}
					%>
				</tbody>
			</table>
		</div>
		<div class="col-md-2"></div>
	</div>

	<%
	} else {
	%>
	<div class="row">
		<div class="col-md-3"></div>

		<div class="col-md-6 text-center mt-5 mb-3">


			<h5>No hay pagos</h5>


		</div>
		<div class="col-md-3"></div>
	</div>

	<%
	}
	%>


	<%
	} else {
	%>
	<div class="row">
		<div class="col-md-3"></div>

		<div class="col-md-6 text-center mt-5 mb-3">


			<h3>No has iniciado sesion</h3>
			<a href="<%=request.getContextPath()%>">Iniciar Sesi�n</a>

		</div>
		<div class="col-md-3"></div>
	</div>

	<%
	}
	%>
</body>
</html>