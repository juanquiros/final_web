<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%
request.getSession().setAttribute("cliente", null);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Cuenta Corriente</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>
	<div class="row  mt-5 ">
		<div class="col-md-4"></div>
		
			<div class="col-md-4 card border-primary mb-3 ml-5" style="max-width: 24rem;">
				<div class="card-header">
					<h1>Iniciar sesi�n</h1>
				</div>
				<div class="card-body">
					<form action="login" method="post">

						<div class="form-group">
							<label for="usuario">Usuario</label> <input type="text"
								class="form-control" id="usuario-web"
								placeholder="nombre de usuario" name="usuario-web" required>
						</div>

						<div class="form-group">
							<label for="password">Contrase�a:</label> <input type="password"
								class="form-control" id="contrasenia"
								placeholder="contrase�a de usuario" name="contrasenia" required>
						</div>
						<%
						if (request.getSession().getAttribute("session") != null && request.getSession().getAttribute("session") == "fail") {
						%>
						<div class="alert alert-warning text-center">
							<p>Usuario o contrase�a incorrecto</p>
						</div>
						<%
						}
						%>
						<button type="submit" class="btn btn-primary mt-2"
							style="width: 100%; height: 4em;">Ingresar</button>
					</form>
				</div>
			</div>
		<div class="col-md-4"></div>







	</div>
</body>
</html>