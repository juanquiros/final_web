package dao;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.Transaction;

import modelo.Cliente;
import modelo.Comprador;
import modelo.Inmueble;
import modelo.Locador;
import modelo.Locatario;


import util.HibernateUtil;

public class webDao {
	private Cliente cliente = null;
	

	public Cliente validar(String usuarioWeb, String contraseniaWeb) {
		
		Locatario locatario = null;
		Comprador comprador = null;

		Transaction transaction = null;
		
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			// start a transaction
			Session sesion= HibernateUtil.getSessionFactory().getCurrentSession();
			transaction = sesion.beginTransaction();	     
		     
			locatario = this.locatario(usuarioWeb);
			comprador = this.getCompradorPorUsuario(usuarioWeb);		
			
			if(locatario!=null ) {
				if(locatario.getContrasenia().equals(contraseniaWeb)) {
					this.cliente = locatario.getCliente();
				}					
			}			
			if( comprador!=null) {		
				if(comprador.getContraseniaWeb().equals(contraseniaWeb)) {
					this.cliente = comprador.getCliente();
				}					
			}			
			if(this.cliente != null) {
				transaction.commit();
				return this.cliente;
			}
			
			
			
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	public Locatario locatario(String usuario) {
		List<Locatario> locatario = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			locatario = session.createQuery("from Locatario where usuarioWeb like :usuario", Locatario.class).setParameter("usuario", usuario).list();
		}
		if(locatario!= null && locatario.size()>0) return locatario.get(0);
		return null;
	}
	
	public Comprador getCompradorPorUsuario(String usuario) {
		List<Comprador> comprador = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {			
			comprador = session.createQuery("from Comprador where usuarioWeb like :usuario", Comprador.class).setParameter("usuario", usuario).list();
		}		
		if(comprador!= null && comprador.size()>0) return comprador.get(0);
		return null;
	}
	
	
}
