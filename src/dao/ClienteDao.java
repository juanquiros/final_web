package dao;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.Transaction;

import modelo.Cliente;
import modelo.Comprador;
import modelo.Garante;
import modelo.Locador;
import modelo.Locatario;
import util.HibernateUtil;


public class ClienteDao {

	public Cliente getClientePorDocumento(String documento) {   
		Cliente cliente = null; 
		if(documento.length()>0){
			 Session sesion= HibernateUtil.getSessionFactory().getCurrentSession();
		     Transaction tx = sesion.beginTransaction();
		     CriteriaBuilder cb = sesion.getCriteriaBuilder();
		     CriteriaQuery<Cliente> cq = cb.createQuery(Cliente.class);
		        Root<Cliente> rootEntry = cq.from(Cliente.class);
		        CriteriaQuery<Cliente> all = cq.select(rootEntry).where(cb.equal(rootEntry.get("documento"),documento)); 
		        TypedQuery<Cliente> allQuery = sesion.createQuery(all);        
		        if(allQuery.getResultList()!=null && !allQuery.getResultList().isEmpty()){
		        	cliente = (Cliente) allQuery.getResultList().get(0);
		            }
		        tx.commit();
		}		
        return cliente ;		
    }
	
	public Locatario getLocatarioPorCliente(Cliente cliente) {   
		Locatario locatario = null; 
		if(cliente!=null){
			 Session sesion= HibernateUtil.getSessionFactory().getCurrentSession();
		     Transaction tx = sesion.beginTransaction();
		     CriteriaBuilder cb = sesion.getCriteriaBuilder();
		     CriteriaQuery<Locatario> cq = cb.createQuery(Locatario.class);
		        Root<Locatario> rootEntry = cq.from(Locatario.class);
		        System.out.println(cliente.getCod_cliente());
		        CriteriaQuery<Locatario> all = cq.select(rootEntry).where(cb.equal(rootEntry.get("cliente"),cliente.getCod_cliente())); 
		        TypedQuery<Locatario> allQuery = sesion.createQuery(all);        
		        if(allQuery.getResultList()!=null && !allQuery.getResultList().isEmpty()){
		        	locatario = (Locatario) allQuery.getResultList().get(0);
		            }
		        tx.commit();
		}		
        return locatario ;		
    }
	

	public Locador getLocadorPorCliente(Cliente cliente) {   
		Locador locador = null; 
		if(cliente!=null){
			 Session sesion= HibernateUtil.getSessionFactory().getCurrentSession();
		     Transaction tx = sesion.beginTransaction();
		     CriteriaBuilder cb = sesion.getCriteriaBuilder();
		     CriteriaQuery<Locador> cq = cb.createQuery(Locador.class);
		        Root<Locador> rootEntry = cq.from(Locador.class);
		        CriteriaQuery<Locador> all = cq.select(rootEntry).where(cb.equal(rootEntry.get("cliente"),cliente.getCod_cliente())); 
		        TypedQuery<Locador> allQuery = sesion.createQuery(all);        
		        if(allQuery.getResultList()!=null && !allQuery.getResultList().isEmpty()){
		        	locador = (Locador) allQuery.getResultList().get(0);
		            }
		        tx.commit();
		}
        return locador ; 
    
    }
	


	public Comprador getCompradorPorCliente(Cliente cliente) {  
		
		
		Comprador comprador = null; 
		if(cliente!=null){
			 Session sesion= HibernateUtil.getSessionFactory().getCurrentSession();
		     Transaction tx = sesion.beginTransaction();
		     CriteriaBuilder cb = sesion.getCriteriaBuilder();
		     CriteriaQuery<Comprador> cq = cb.createQuery(Comprador.class);
		        Root<Comprador> rootEntry = cq.from(Comprador.class);
		        CriteriaQuery<Comprador> all = cq.select(rootEntry).where(cb.equal(rootEntry.get("cliente"),cliente.getCod_cliente())); 
		        TypedQuery<Comprador> allQuery = sesion.createQuery(all);        
		        if(allQuery.getResultList()!=null && !allQuery.getResultList().isEmpty()){
		        	comprador = (Comprador) allQuery.getResultList().get(0);
		            }
		        tx.commit();
		}
        return comprador ; 

             
    }
	
	public Garante getGarantePorCliente(Cliente cliente) {  
		Garante garante = null; 
		if(cliente!=null){
			 Session sesion= HibernateUtil.getSessionFactory().getCurrentSession();
		     Transaction tx = sesion.beginTransaction();
		     CriteriaBuilder cb = sesion.getCriteriaBuilder();
		     CriteriaQuery<Garante> cq = cb.createQuery(Garante.class);
		        Root<Garante> rootEntry = cq.from(Garante.class);
		        CriteriaQuery<Garante> all = cq.select(rootEntry).where(cb.equal(rootEntry.get("cliente"),cliente.getCod_cliente())); 
		        TypedQuery<Garante> allQuery = sesion.createQuery(all);        
		        if(allQuery.getResultList()!=null && !allQuery.getResultList().isEmpty()){
		        	garante = (Garante) allQuery.getResultList().get(0);
		            }
		        tx.commit();
		}
        return garante ;       
    }
	
	
	
	public List<Cliente> getClientes() {
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			 TypedQuery<Cliente> allQuery = session.createQuery("from Cliente", Cliente.class);      
		        if(allQuery.getResultList()!=null && !allQuery.getResultList().isEmpty()){
		        	return  allQuery.getResultList();
		            }else{
		            	return null;
		            }
		}
	}
	public List<Locatario> getLocatarios() {
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			 TypedQuery<Locatario> allQuery = session.createQuery("from Locatario", Locatario.class);      
		        if(allQuery.getResultList()!=null && !allQuery.getResultList().isEmpty()){
		        	return  allQuery.getResultList();
		            }else{
		            	return null;
		            }
		}
	}
	
	public List<Locador> getLocador() {
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			 TypedQuery<Locador> allQuery = session.createQuery("from Locador", Locador.class);      
		        if(allQuery.getResultList()!=null && !allQuery.getResultList().isEmpty()){
		        	return  allQuery.getResultList();
		            }else{
		            	return null;
		            }
		}
	}
	public List<Comprador> getComprador() {
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {		
			
			 TypedQuery<Comprador> allQuery = session.createQuery("from Comprador", Comprador.class);      
		        if(allQuery.getResultList()!=null && !allQuery.getResultList().isEmpty()){
		        	return  allQuery.getResultList();
		            }else{
		            	return null;
		            }
		}
	}
	public List<Garante> getGarante() {
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {						
			 TypedQuery<Garante> allQuery = session.createQuery("from Garante", Garante.class);      
			 if(allQuery.getResultList()!=null && !allQuery.getResultList().isEmpty()){
		        	return  allQuery.getResultList();
		            }else{
		            	return null;
		            }
		}
	}
}


