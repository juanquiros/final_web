package dao;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.Transaction;

import modelo.Venta;
import util.HibernateUtil;

public class VentasDao {

	public Venta getVentarPorInmueble(String documento_) {   
		Venta venta = null; 
        Session sesion= HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = sesion.beginTransaction();
        CriteriaBuilder cb = sesion.getCriteriaBuilder();
        CriteriaQuery<Venta> cq = cb.createQuery(Venta.class);
        Root<Venta> rootEntry = cq.from(Venta.class);
        CriteriaQuery<Venta> all = cq.select(rootEntry).where(cb.equal(rootEntry.get("documento"),documento_)); 
        TypedQuery<Venta> allQuery = sesion.createQuery(all);        
        if(allQuery.getResultList()!=null && !allQuery.getResultList().isEmpty()){
        	venta = (Venta) allQuery.getResultList().get(0);
            }
        tx.commit();
        return venta ;       
    }
	public List<Venta> getAlquileres() {
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			return session.createQuery("from Alquileres", Venta.class).list();
		}
	}
}
