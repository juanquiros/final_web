package util;

import java.util.Properties;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;



/**
 * Java based configuration
 * @author ramesh Fadatare
 *
 */
public class HibernateUtil {
	private static SessionFactory sessionFactory;

	public static SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			try {
				Configuration configuration = new Configuration();

				// Hibernate settings equivalent to hibernate.cfg.xml's properties
				Properties settings = new Properties();
				settings.put(Environment.DRIVER, "com.mysql.jdbc.Driver");
				settings.put(Environment.URL, "jdbc:mysql://localhost:3306/admin_tp_final_PAII?serverTimezone=UTC");
				settings.put(Environment.USER, "root");
				settings.put(Environment.PASS, "Juanjuan09");
				settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5Dialect");

				settings.put(Environment.SHOW_SQL, "true");

				settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");

				settings.put(Environment.HBM2DDL_AUTO, "validate");

				configuration.setProperties(settings);
				
				configuration.addAnnotatedClass(modelo.Alquiler.class);
				configuration.addAnnotatedClass(modelo.ArancelEspecial.class);
				configuration.addAnnotatedClass(modelo.Casa.class);
				configuration.addAnnotatedClass(modelo.Cliente.class);
				configuration.addAnnotatedClass(modelo.ComicionInmobiliaria.class);
				configuration.addAnnotatedClass(modelo.Comprador.class);
				configuration.addAnnotatedClass(modelo.Contrato.class);
				configuration.addAnnotatedClass(modelo.Departamento.class);
				configuration.addAnnotatedClass(modelo.Garante.class);
				configuration.addAnnotatedClass(modelo.Inmueble.class);
				configuration.addAnnotatedClass(modelo.Locador.class);
				configuration.addAnnotatedClass(modelo.LocalComercial.class);
				configuration.addAnnotatedClass(modelo.Locatario.class);
				configuration.addAnnotatedClass(modelo.Pago.class);
				configuration.addAnnotatedClass(modelo.Recargo.class);
				configuration.addAnnotatedClass(modelo.Terreno.class);
				configuration.addAnnotatedClass(modelo.Venta.class);

				ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
						.applySettings(configuration.getProperties()).build();
				System.out.println("Hibernate Java Config serviceRegistry created");
				sessionFactory = configuration.buildSessionFactory(serviceRegistry);
				return sessionFactory;

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return sessionFactory;
	}
}
