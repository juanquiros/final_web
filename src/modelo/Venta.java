package modelo;

import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@PrimaryKeyJoinColumn(name="fk_contrato")
@Table(name="Venta")
public class Venta extends Contrato implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	
	
	@ManyToOne
	@JoinColumn(name="fk_terreno")
	private Terreno terrenoVendido;
	@ManyToOne
	@JoinColumn(name="fk_casa")
	private Casa casaVendida;
	@ManyToOne
	@JoinColumn(name="FK_comprador")
    private Comprador comprador;
	
	public Venta(){}
	
	
	public Venta(LocalDate fechaRealizado, int duracion, boolean habilitado, ComicionInmobiliaria comicionInmobiliari,
			Recargo unRecargo, Casa casaVendida, Terreno terrenoVendido,
			Comprador comprador) {
		super(fechaRealizado, duracion, habilitado, comicionInmobiliari, unRecargo);
		this.casaVendida = casaVendida;
		this.terrenoVendido = terrenoVendido;
		this.comprador = comprador;
	}
	public Casa getCasaVendida() {
		return casaVendida;
	}
	public void setCasaVendida(Casa casaVendida) {
		this.casaVendida = casaVendida;
	}
	public Terreno getTerrenoVendido() {
		return terrenoVendido;
	}
	public void setTerrenoVendido(Terreno terrenoVendido) {
		this.terrenoVendido = terrenoVendido;
	}
	public Comprador getComprador() {
		return comprador;
	}
	public void setComprador(Comprador comprador) {
		this.comprador = comprador;
	}
	
}
