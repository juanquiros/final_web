package modelo;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import excepcion.BadPostCodeException;

@Entity
@Table(name="Garantes")
public class Garante implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="cod_garante", unique=true,updatable=false,nullable =false)
 	private int cod_garante;
	
	
	@OneToOne
	@JoinColumn(name="cliente_cod_cliente")
    private Cliente cliente;
	
	@Column(name="activadaQueRealiza")
	private String activadaQueRealiza;
	
	@Column(name="comprobanteIngresos")
    private String comprobanteIngresos;    
	
	@OneToMany(mappedBy="garante", cascade=CascadeType.REMOVE)
	private List<Alquiler> alquiler;
	
	
	public Garante() {}	
	public Garante(Cliente cliente, String activadaQueRealiza, String comprobanteIngresos) throws BadPostCodeException {
		this.cliente = cliente;
		this.activadaQueRealiza = activadaQueRealiza;
		this.comprobanteIngresos = comprobanteIngresos;
		
		if(!validarActividadGarante()) {
			throw new BadPostCodeException("El campo de  actividad que realiza el Garante   no puede estar vacio.");
		}else if(!validarComprobanteIngreso()){
			throw new BadPostCodeException("\"El campo de comprobante de ingresos no puede estar vacio.");
		}
	}

	public String getActivadaQueRealiza() {
		return activadaQueRealiza;
	}
	public void setActivadaQueRealiza(String activadaQueRealiza) {
		this.activadaQueRealiza = activadaQueRealiza;
	}
	public String getComprobanteIngresos() {
		return comprobanteIngresos;
	}
	public void setComprobanteIngresos(String comprobanteIngresos) {
		this.comprobanteIngresos = comprobanteIngresos;
	}
    public String toString(){
    	return "Datos garante: \n" + this.getComprobanteIngresos() + ", " + this.getActivadaQueRealiza();
    	
    }
    
    
    public int getCod_garante() {
		return cod_garante;
	}
	public void setCod_garante(int cod_garante) {
		this.cod_garante = cod_garante;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public List<Alquiler> getAlquiler() {
		return alquiler;
	}
	public void setAlquiler(List<Alquiler> alquiler) {
		this.alquiler = alquiler;
	}
	/*-----------------------Metodos de validar Garante-------------------------------------*/
    public boolean validarActividadGarante(){
 		boolean valido = false;
 		if(this.activadaQueRealiza.length()>6 && this.activadaQueRealiza.length()<20){
 			valido = true;
 		}
 		return valido;
 	}
    
    public boolean validarComprobanteIngreso(){
 		boolean valido = false;
 		if(this.comprobanteIngresos.length()>6 && this.comprobanteIngresos.length()<20){
 			valido = true;
 		}
 		return valido;
 	}
}
