package modelo;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import excepcion.BadPostCodeException;

import javax.persistence.InheritanceType;
import javax.persistence.OneToOne;

@Entity
@Table(name ="Clientes", uniqueConstraints={
		@UniqueConstraint(columnNames="documento")
})

@Inheritance(strategy = InheritanceType.JOINED)
public class Cliente  implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="cod_cliente", unique=true,updatable=false,nullable =false)
 	private int cod_cliente;
	
	@Column(name="tipoDocumento")
 	private String tipoDeDocumento;
	@Column(name="documento")
	private String documento;
	@Column(name="nombre") 	
	private String nombre;
	@Column(name="apellido") 	
	private String apellido;
	@Column(name="estadoCivil")
 	private String estadoCivil;
	@Column(name="domicilio")
 	private String domicilio;
	@Column(name="telefono")
 	private String telefono;
	@Column(name="email")
 	private String email;
	
	@OneToOne(mappedBy="cliente",cascade=CascadeType.REMOVE)
	private Comprador comprador;
	
	@OneToOne(mappedBy="cliente",cascade=CascadeType.REMOVE)
	private Locatario locatario;
	
	@OneToOne(mappedBy="cliente",cascade=CascadeType.REMOVE)
	private Garante garante;
	
	@OneToOne(mappedBy="cliente",cascade=CascadeType.REMOVE)
	private Locador locador;
	 	
	public Cliente(){}
		
		public Cliente(String tipoDeDocumento, String documento, String nombre, String apellido,
				String estadoCivil, String domicilio, String telefono, String email) throws BadPostCodeException{
			this.tipoDeDocumento = tipoDeDocumento;
			this.documento = documento;
			this.nombre = nombre;
			this.apellido = apellido;
			this.estadoCivil = estadoCivil;
			this.domicilio = domicilio;
			this.telefono = telefono;
			this.email = email;
			
			if(!validarDocumento()) {
				throw new BadPostCodeException("El documento debe tener mas de 6 digitos y menos de 9, no puede estar vac�o");
			}else if(!validarTipoDocumento()) {
				throw new BadPostCodeException("El campo de Tipo de documento  no puede estar vacio.");
			}else if(!validarNombre()) {
				throw new BadPostCodeException("El campo de Nombre  no puede estar vacio.");
			}else if(!validarApellido()){
				throw new BadPostCodeException("El campo de Apellido  no puede estar vacio.");
			}else if(!validarEstadoCivil()) {
				throw new BadPostCodeException("El campo de Estado Civil  no puede estar vacio.");
			}else if(!validarDomicilio()) {
				throw new BadPostCodeException("El campo de Domicilio  no puede estar vacio.");
			}else if(!validarTelefono()) {
				throw new BadPostCodeException("El campo de Telefono  no puede estar vacio.");
			}else if(!validarEmail()) {
				throw new BadPostCodeException("El campo de Email  no puede estar vacio.");
			}
		}
		public int getCod_cliente() {
			return cod_cliente;
		}
		public void setCod_cliente(int cod_cliente) {
			this.cod_cliente = cod_cliente;
		}
		public String getTipoDeDocumento() {
			return tipoDeDocumento;
		}
		public void setTipoDeDocumento(String tipoDeDocumento) {
			this.tipoDeDocumento = tipoDeDocumento;
		}
		public String getDocumento() {
			return documento;
		}
		public void setDocumento(String documento) {
			this.documento = documento;
		}
		public String getNombre() {
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		public String getApellido() {
			return apellido;
		}
		public void setApellido(String apellido) {
			this.apellido = apellido;
		}
		public String getEstadoCivil() {
			return estadoCivil;
		}
		public void setEstadoCivil(String estadoCivil) {
			this.estadoCivil = estadoCivil;
		}
		public String getDomicilio() {
			return domicilio;
		}
		public void setDomicilio(String domicilio) {
			this.domicilio = domicilio;
		}
		public String getTelefono() {
			return telefono;
		}
		public void setTelefono(String telefono) {
			this.telefono = telefono;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
	 	public String toString(){
	 		return getApellido() + " " + getNombre() + ", " + getTipoDeDocumento()+":"+getDocumento()+", "+ getEstadoCivil() +","+getTelefono()+", "+getDomicilio()+", " + getEmail();	 		
	 	}
	 	
	
	 	/*----------------metodos de validacion de cliente ------------------------*/
	 	
	 	public boolean validarDocumento(){
	 		boolean valido = false;
	 		if(this.documento.length()>6 && this.documento.length()<9){
	 			valido = true;
	 		}
	 		return valido;
	 	}
	 	
	 	public boolean validarTipoDocumento() {
	 		boolean valido = false;
	 		if(this.tipoDeDocumento.length()>0 && this.tipoDeDocumento.length()<=20){
	 			valido = true;
	 		}
	 		return valido;
	 		
	 	}
	 	
	 	public boolean validarNombre() {
	 		boolean valido = false;
	 		if(this.nombre.length()>0 && this.nombre.length()<=20){
	 			valido = true;
	 		}
	 		return valido;
	 		
	 	}
	 	
	 	public boolean validarApellido() {
	 		boolean valido = false;
	 		if(this.apellido.length()>0 && this.apellido.length()<=20){
	 			valido = true;
	 		}
	 		return valido;
	 		
	 	}
	 	
	 	public boolean validarEstadoCivil() {
	 		boolean valido = false;
	 		if(this.estadoCivil.length()>0 && this.estadoCivil.length()<=20){
	 			valido = true;
	 		}
	 		return valido;
	 		
	 	}
	 	
	 	public boolean validarDomicilio() {
	 		boolean valido = false;
	 		if(this.domicilio.length()>0 && this.domicilio.length()<=20){
	 			valido = true;
	 		}
	 		return valido;
	 		
	 	}
	 	
	 	public boolean validarTelefono() {
	 		boolean valido = false;
	 		if(this.telefono.length()>0 && this.telefono.length()<=20){
	 			valido = true;
	 		}
	 		return valido;
	 		
	 	}
	 	
	 	public boolean validarEmail() {
	 		boolean valido = false;
	 		if(this.email.length()>0 && this.email.length()<=30){
	 			valido = true;
	 		}
	 		return valido;
	 		
	 	}
	 /*----------------------------------------------------------------------------------------*/
}
