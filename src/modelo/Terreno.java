package modelo;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@PrimaryKeyJoinColumn(name="fk_inmueble")
@Table(name="Terreno")
public class Terreno extends Inmueble implements java.io.Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	 @OneToMany(mappedBy="terrenoVendido")
	 private Set<Venta> ventas;
	public Terreno(){}
	public Terreno(String ubicacion, int metrosCuadrados, boolean habilidato, Locador unLocador){
		super(ubicacion,metrosCuadrados,habilidato,unLocador);
	}
	public String toString(){
		return this.descripcionInmueble() + " Tipo: Terreno ";
	}
}
