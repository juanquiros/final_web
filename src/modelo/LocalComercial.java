package modelo;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@PrimaryKeyJoinColumn(name="fk_inmueble")
@Table(name="LocalesComerciales")
public class LocalComercial extends Inmueble implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@OneToMany(mappedBy="localComercialAlquilado",cascade=CascadeType.REMOVE)
	private Set<Alquiler> alquiler;
	public LocalComercial(){}
	public LocalComercial(String ubicacion, int metrosCuadrados, boolean habilidato, Locador unLocador){
		super(ubicacion,metrosCuadrados,habilidato,unLocador);
	}
	
	
	public String toString(){
		return this.descripcionInmueble() + " Tipo: Local Comercial ";
	}
}
