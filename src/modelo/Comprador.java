package modelo;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import excepcion.BadPostCodeException;
@Entity
@Table(name="Comprador")
public class Comprador implements java.io.Serializable  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="cod_comprador", unique=true,updatable=false,nullable =false)
 	private int cod_comprador;
    
	@OneToOne
	@JoinColumn(name="cliente_cod_cliente")
    private Cliente cliente;
	
	
	@Column(name="activadaQueRealiza")
	 	private String activadaQueRealiza;
	@Column(name="comprobanteIngresos")
		private String comprobanteIngresos;
	@Column(name="usuarioWeb")
	    private String usuarioWeb;
	@Column(name="contraseniaWeb")
	    private String contraseniaWeb;
	
	
	@OneToMany(mappedBy="comprador")
	private Set<Venta> venta;
	
	public Comprador(){}

	public Comprador(Cliente cliente, String activadaQueRealiza, String comprobanteIngresos, String usuarioWeb, String contraseniaWeb) throws BadPostCodeException {
		this.cliente = cliente;
		this.activadaQueRealiza = activadaQueRealiza;
		this.comprobanteIngresos = comprobanteIngresos;
		this.usuarioWeb = usuarioWeb;
		this.contraseniaWeb = contraseniaWeb;
		
		if(!validarActividadQueRealiza()) {
			throw new BadPostCodeException("El campo de Actividad Que Realiza  no puede estar vacio.Entre 6 y 20 caracteres");
		}else if(!validarComprobanteIngreso()) {
			throw new BadPostCodeException("El campo de Comprobante de ingresos  no puede estar vacio.Entre 6 y 20 caracteres");
		}else if(!validarUsuarioWeb()) {
			throw new BadPostCodeException("El campo de Usuario Web  no puede estar vacio.Entre 2 y 9 caracteres");
		}else if(!validarContraseniaWeb()) {
			throw new BadPostCodeException("El campo de Contrasenia  no puede estar vacio.Entre 6 y 9 caracteres");
		}
		
	}

	public int getCod_comprador() {
		return cod_comprador;
	}

	public void setCod_comprador(int cod_comprador) {
		this.cod_comprador = cod_comprador;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}



	public String getActivadaQueRealiza() {
		return activadaQueRealiza;
	}

	public void setActivadaQueRealiza(String activadaQueRealiza) {
		this.activadaQueRealiza = activadaQueRealiza;
	}

	public String getComprobanteIngresos() {
		return comprobanteIngresos;
	}

	public void setComprobanteIngresos(String comprobanteIngresos) {
		this.comprobanteIngresos = comprobanteIngresos;
	}

	public String getUsuarioWeb() {
		return usuarioWeb;
	}

	public void setUsuarioWeb(String usuarioWeb) {
		this.usuarioWeb = usuarioWeb;
	}

	public String getContraseniaWeb() {
		return contraseniaWeb;
	}

	public void setContraseniaWeb(String contraseniaWeb) {
		this.contraseniaWeb = contraseniaWeb;
	}
	
	
	public Set<Venta> getVenta() {
		return venta;
	}

	public void setVenta(Set<Venta> venta) {
		this.venta = venta;
	}

	public String toString(){
		return "Datos de Comprador: \n" + this.getUsuarioWeb() + ", " + this.getContraseniaWeb() + ", " + this.getComprobanteIngresos() +
				", " + this.getActivadaQueRealiza();
	
	}

	/*----------------metodos de validacion de comprador ------------------------*/
	
	public boolean validarActividadQueRealiza(){
 		boolean valido = false;
 		if(this.activadaQueRealiza.length()>6 && this.activadaQueRealiza.length()<20){
 			valido = true;
 		}
 		return valido;
 	}
	
	public boolean validarComprobanteIngreso(){
 		boolean valido = false;
 		if(this.comprobanteIngresos.length()>6 && this.comprobanteIngresos.length()<20){
 			valido = true;
 		}
 		return valido;
 	}
	
	public boolean validarUsuarioWeb(){
 		boolean valido = false;
 		if(this.usuarioWeb.length()>2 && this.usuarioWeb.length()<9){
 			valido = true;
 		}
 		return valido;
 	}
	public boolean validarContraseniaWeb(){
 		boolean valido = false;
 		if(this.contraseniaWeb.length()>6 && this.contraseniaWeb.length()<9){
 			valido = true;
 		}
 		return valido;
 	}
}
