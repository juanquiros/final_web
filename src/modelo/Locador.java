package modelo;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import java.util.List;
import excepcion.BadPostCodeException;

@Entity
@Table(name="Locador")
public class Locador implements java.io.Serializable {
	
	  	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="cod_locador", unique=true,updatable=false,nullable =false)
 	private int cod_locador;
	
	
	@OneToOne
	@JoinColumn(name="cliente_cod_cliente")
    private Cliente cliente;
	
	
	@Column(name="cuit")
		private String cuit;
	
	@Column(name="razonSocial")
	    private String razonSocial;
	
	 @OneToMany(mappedBy="locador",cascade=CascadeType.REMOVE)
	private List<Inmueble> inmueble;
	
	public Locador(){}
	public Locador(Cliente cliente, String cuit, String razonSocial) throws BadPostCodeException {
		this.cliente = cliente;
		this.cuit = cuit;
		this.razonSocial = razonSocial;
		
		if(!validarCuit()) {
			throw new BadPostCodeException("\"El campo de CUIT no puede estar vacio.");
		}else if(!validarRazonSocial()) {
			throw new BadPostCodeException("El campo de razon social no puede estar vacio.");
		}
		
	}
	
	public int getCod_locador() {
		return cod_locador;
	}
	public void setCod_locador(int cod_locador) {
		this.cod_locador = cod_locador;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public List<Inmueble> getInmueble() {
		return inmueble;
	}
	public void setInmueble(List<Inmueble> inmueble) {
		this.inmueble = inmueble;
	}
	public String getCuit() {
		return cuit;
	}
	public void setCuit(String cuit) {
		this.cuit = cuit;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	public String toString(){
		return "Datos de Locador: \n" + this.cuit +", " + this.razonSocial;
	//String cuit, String razonSocial
	}
	
	
	
	
	/*------------------Metodos de validar Locador--------------------------------------*/
	public boolean validarCuit(){
 		boolean valido = false;
 		if(this.cuit.length()>6 && this.cuit.length()<12){
 			valido = true;
 		}
 		return valido;
 	}
	public boolean validarRazonSocial(){
 		boolean valido = false;
 		if(this.razonSocial.length()>6 && this.razonSocial.length()<20){
 			valido = true;
 		}
 		return valido;
 	}
	    
	    
}
