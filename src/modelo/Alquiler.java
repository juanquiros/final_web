package modelo;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;


@Entity
@PrimaryKeyJoinColumn(name="fk_contrato")
@Table(name="Alquileres")
@Inheritance(strategy = InheritanceType.JOINED)
public class Alquiler extends Contrato implements java.io.Serializable {
	
	
	//Inmueble
	private static final long serialVersionUID = 1L;
	
	@ManyToOne
	@JoinColumn(name="Casa_cod_casa")
	private Casa casaAlquilada;
	
	@ManyToOne
	@JoinColumn(name="Departamento_cod_Departamento")
	private Departamento departamentoAlquilado;
	
	@ManyToOne
	@JoinColumn(name="LocalComercial_LocalComercial")
	private LocalComercial localComercialAlquilado;
	
	
	//garantes
	@ManyToOne
	@JoinColumn(name="garante_cod_garante")
    private Garante garante;
	
	//locatario
	@ManyToOne
	@JoinColumn(name="locatario_cod_locatario")
    private Locatario locatario;
    
    
    
    public Alquiler(){}
	public Alquiler(LocalDate fechaRealizado, int duracion, boolean habilitado,
			ComicionInmobiliaria comicionInmobiliari, Recargo unRecargo,
			Casa casaAlquilada, Departamento departamentoAlquilado, LocalComercial localComercialAlquilado,
			Garante garante, Locatario locatario) {
		super(fechaRealizado, duracion, habilitado, comicionInmobiliari, unRecargo);
		this.casaAlquilada = casaAlquilada;
		this.departamentoAlquilado = departamentoAlquilado;
		this.localComercialAlquilado = localComercialAlquilado;
		this.garante = garante;
		this.locatario = locatario;
	}
	public Casa getCasaAlquilada() {
		return casaAlquilada;
	}
	public void setCasaAlquilada(Casa casaAlquilada) {
		this.casaAlquilada = casaAlquilada;
	}
	public Departamento getDepartamentoAlquilado() {
		return departamentoAlquilado;
	}
	public void setDepartamentoAlquilado(Departamento departamentoAlquilado) {
		this.departamentoAlquilado = departamentoAlquilado;
	}
	public LocalComercial getLocalComercialAlquilado() {
		return localComercialAlquilado;
	}
	public void setLocalComercialAlquilado(LocalComercial localComercialAlquilado) {
		this.localComercialAlquilado = localComercialAlquilado;
	}
	public Garante getGarante() {
		return garante;
	}
	public void setGarante(Garante garante) {
		this.garante = garante;
	}
	public Locatario getLocatario() {
		return locatario;
	}
	public void setLocatario(Locatario locatario) {
		this.locatario = locatario;
	} 
	
	public String toString(){
		String inmueble="";
		if(casaAlquilada!=null){
			inmueble = casaAlquilada.toString();
		}
		return "Contrato: " + this.decripcionContrato() + "\n" + locatario.toString() + "\n" + garante.toString() + "\n" + inmueble;
	}
}
