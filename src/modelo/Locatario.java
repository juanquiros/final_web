package modelo;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import excepcion.BadPostCodeException;
@Entity
@PrimaryKeyJoinColumn(name="fk_cliente")
@Table(name="Locatario")
public class Locatario implements java.io.Serializable  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="cod_locatario", unique=true,updatable=false,nullable =false)
 	private int cod_locatario;
	
	
	@OneToOne
	@JoinColumn(name="cliente_cod_cliente")
    private Cliente cliente;
	
	
	@Column(name="actividadQueRealiza")
	 	private String actividadQueRealiza;
	@Column(name="comprobanteIngreso")
	    private String comprobanteIngreso; //descripcion de que papel entrego
	@Column(name="usuarioWeb")
	    private String usuarioWeb;
	@Column(name="contrasenia")
	    private String contrasenia;
	
	@OneToMany(mappedBy="locatario",cascade=CascadeType.REMOVE)
	private List<Alquiler>  alquiler;
	
	
	
	public Locatario(){}
	public Locatario(Cliente cliente, String actividadQueRealiza, String comprobanteIngreso, String usuarioWeb, String contrasenia) throws BadPostCodeException {
		this.cliente = cliente;
		this.actividadQueRealiza = actividadQueRealiza;
		this.comprobanteIngreso = comprobanteIngreso;
		this.usuarioWeb = usuarioWeb;
		this.contrasenia = contrasenia;
		
		if(!validarActividadLocatario()) {
			throw new BadPostCodeException("El campo de  actividad que realiza el Locatario   no puede estar vacio.");
		}else if(!validarComprobanteIngreso()) {
			throw new BadPostCodeException("\"El campo de comprobante de ingresos no puede estar vacio.");
		}else if(!validarUsuarioWeb()) {
			throw new BadPostCodeException("El campo de usuario web no puede estar vacio.");
		}else if(!validarContraseniaWeb()) {
			throw new BadPostCodeException("El campo de contraseniano puede estar vacio.");
		}
		
		
	}
	
	public int getCod_locatario() {
		return cod_locatario;
	}
	public void setCod_locatario(int cod_locatario) {
		this.cod_locatario = cod_locatario;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public List<Alquiler> getAlquiler() {
		return alquiler;
	}
	public void setAlquiler(List<Alquiler> alquiler) {
		this.alquiler = alquiler;
	}

	public String getActividadQueRealiza() {
		return actividadQueRealiza;
	}
	public void setActividadQueRealiza(String actividadQueRealiza) {
		this.actividadQueRealiza = actividadQueRealiza;
	}
	public String getComprobanteIngreso() {
		return comprobanteIngreso;
	}
	public void setComprobanteIngreso(String comprobanteIngreso) {
		this.comprobanteIngreso = comprobanteIngreso;
	}
	public String getUsuarioWeb() {
		return usuarioWeb;
	}
	public void setUsuarioWeb(String usuarioWeb) {
		this.usuarioWeb = usuarioWeb;
	}
	public String getContrasenia() {
		return contrasenia;
	}
	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}
	public String toString(){
		return "Datos de locatario: \n" + this.actividadQueRealiza + ", " + this.comprobanteIngreso +
				", " + this.usuarioWeb + ", " + this.contrasenia;
	//String actividadQueRealiza, String comprobanteIngreso, String usuarioWeb, String contrasenia
	}
	
	
	
	/*--------------------------Metodos de validar Locatario-----------------------------------------------*/
	public boolean validarActividadLocatario(){
 		boolean valido = false;
 		if(this.actividadQueRealiza.length()>6 && this.actividadQueRealiza.length()<20){
 			valido = true;
 		}
 		return valido;
 	}
	
	public boolean validarComprobanteIngreso(){
 		boolean valido = false;
 		if(this.comprobanteIngreso.length()>6 && this.comprobanteIngreso.length()<20){
 			valido = true;
 		}
 		return valido;
 	}
	
	public boolean validarUsuarioWeb(){
 		boolean valido = false;
 		if(this.usuarioWeb.length()>6 && this.usuarioWeb.length()<9){
 			valido = true;
 		}
 		return valido;
 	}
	public boolean validarContraseniaWeb(){
 		boolean valido = false;
 		if(this.contrasenia.length()>6 && this.contrasenia.length()<9){
 			valido = true;
 		}
 		return valido;
 	}
	
	
}
