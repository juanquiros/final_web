package controlador;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import dao.AlquilerDao;
import dao.ClienteDao;
import dao.CuentaCorrienteClienteDao;
import dao.Dao;
import dao.InmuebleDao;
import dao.VentaDao;
import excepcion.BadPostCodeException;
import modelo.Alquiler;
import modelo.ArancelEspecial;
import modelo.Casa;
import modelo.Cliente;
import modelo.Comprador;
import modelo.Contrato;
import modelo.Departamento;
import modelo.Garante;
import modelo.Inmueble;
import modelo.Locador;
import modelo.LocalComercial;
import modelo.Locatario;
import modelo.Pago;
import modelo.Terreno;
import modelo.Venta;
import util.HibernateUtil;

public class Controlador {
	InmuebleDao persistenciaInmuebles = new InmuebleDao();
	AlquilerDao persistenciaAlquiler = new AlquilerDao();
	ClienteDao persistenciaCliente = new ClienteDao ();
	CuentaCorrienteClienteDao persistenciaCCcliente = new CuentaCorrienteClienteDao();
	VentaDao persistenciaVenta = new VentaDao();
	Dao dao = new Dao ();
public Controlador() {
	HibernateUtil.getSessionFactory();
}
public Cliente obtenerCliente(String documento_) {
	Cliente cliente = persistenciaCliente.getClientePorDocumento(documento_);
	return cliente;
}
public Locador obtenerLocadorPorDocumento(String documento_) {
	Cliente cliente = persistenciaCliente.getClientePorDocumento(documento_);
	Locador unLocador = persistenciaCliente.getLocadorPorCliente(cliente);
	return unLocador;
}
public Locatario obtenerLocatarioPorDocumento(String documento_) {
	Cliente cliente = persistenciaCliente.getClientePorDocumento(documento_);
	Locatario locatario = persistenciaCliente.getLocatarioPorCliente(cliente);
	return locatario;
}
public Garante obtenerGarantePorDocumento(String documento_) {
	Cliente cliente = persistenciaCliente.getClientePorDocumento(documento_);
	Garante garante = persistenciaCliente.getGarantePorCliente(cliente);
	return garante;
}
public Comprador obtenerCompradorPorDocumento(String documento_) {
	Cliente cliente = persistenciaCliente.getClientePorDocumento(documento_);
	Comprador comprador = persistenciaCliente.getCompradorPorCliente(cliente);
	return comprador;
}
public  ArrayList<Cliente> obtenerTodosLosClientes(){
	ArrayList<Cliente> clientes = new ArrayList<Cliente>();
	clientes.addAll(this.persistenciaCliente.getClientes());	
	return clientes;
}
/*------------------metodos dao de Cliente--------------------------------------*/
	public void  agregarCliente(Object cliente) throws BadPostCodeException {
		if(obtenerCliente(((Cliente) cliente).getDocumento())!=null){
			throw new BadPostCodeException("Ya se encuentra cargado este cliente");//BadPostCodeException
		}else{
			dao.Guardar(cliente);
		}		
	}
	public void  agregarTipoCliente(Object cliente) throws BadPostCodeException {
		
			dao.Guardar(cliente);
				
	}
	public void actualizarCliente(Object Cliente){
		dao.update(Cliente);
	}
	public void actualizarPago(Object pago){
		dao.update(pago);
	}
		public void borrarCliente(Object cliente){
		dao.delete(cliente);		
	}
/*------------------------------------------------------------------------------*/
	public ArrayList<Inmueble> obtenerTodosLosInmuebles() {
		ArrayList<Inmueble> Inmuebles = new ArrayList<Inmueble>();
		ArrayList<Casa> casas = (ArrayList<Casa>) persistenciaInmuebles.getCasas();
		ArrayList<Departamento> departamentos = (ArrayList<Departamento>) persistenciaInmuebles.getDepartamentos();
		ArrayList<LocalComercial> localesComerciales = (ArrayList<LocalComercial>) persistenciaInmuebles
				.getLocalesComerciales();
		ArrayList<Terreno> terrenos = (ArrayList<Terreno>) persistenciaInmuebles.getTerrenos();
		if (casas != null)
			for (Casa unaCasa : casas)
				Inmuebles.add(unaCasa);
		if (departamentos != null)
			for (Departamento unDepartamento : departamentos)
				Inmuebles.add(unDepartamento);
		if (localesComerciales != null)
			for (LocalComercial unLocalComercial : localesComerciales)
				Inmuebles.add(unLocalComercial);
		if (terrenos != null)
			for (Terreno unTerreno : terrenos)
				Inmuebles.add(unTerreno);
		return Inmuebles;
	}

	public ArrayList<Inmueble> obtenerInmueblePorM2yTipo(int min, int max, String tipoInmueble) {
		ArrayList<Inmueble> inmuebleEncontrados= new ArrayList<Inmueble>();

		ArrayList<Casa> casas= null;
		ArrayList<Departamento> departamentos = null;
		ArrayList<LocalComercial> localesComerciales = null;
		ArrayList<Terreno> terrenos = null;
		
		if (tipoInmueble.equals("Casa")) {
			casas = (ArrayList<Casa>) persistenciaInmuebles.getCasaPorM2(min, max);
			if (casas != null)
				for (Casa unaCasa : casas)
					inmuebleEncontrados.add(unaCasa);
		} else if (tipoInmueble.equals("Departamento")) {
			departamentos = (ArrayList<Departamento>) persistenciaInmuebles.getDepartamentoPorM2(min, max);
			if (departamentos != null)
				for (Departamento unDepartamento : departamentos)
					inmuebleEncontrados.add(unDepartamento);
		} else if (tipoInmueble.equals("Local Comercial")) {
			localesComerciales = (ArrayList<LocalComercial>) persistenciaInmuebles.getLocalComercialPorM2(min, max);
			if (localesComerciales != null)
				for (LocalComercial unLocalComercial : localesComerciales)
					inmuebleEncontrados.add(unLocalComercial);
		} else if (tipoInmueble.equals("Terreno")) {
			terrenos = (ArrayList<Terreno>) persistenciaInmuebles.getTerrenoPorM2(min, max);
			if (terrenos != null)
				for (Terreno unTerreno : terrenos)
					inmuebleEncontrados.add(unTerreno);
		} else {
			inmuebleEncontrados = null;
		}
		
		
		
		return inmuebleEncontrados;
	}

	public void agregarInmueble(Object inmueble) {
		dao.Guardar(inmueble);
	}
	
	public void registrarContrato(Contrato contrato) {
		dao.Guardar(contrato);
	}
	public void registrarArancel(ArancelEspecial contrato) {
		dao.Guardar(contrato);
	}
	public void registrarCuotas(Pago pagos) {
		dao.Guardar(pagos);
	}
	
	public void actualizarInmueble(Object inmueble) {
		dao.update(inmueble);
	}

	public void borrarInmueble(Object inmueble) {
		dao.delete(inmueble);
	}
	public ArrayList<Alquiler> getAlquileres(){
		return (ArrayList<Alquiler>) this.persistenciaAlquiler.getAlquileres();
	}
	public ArrayList<Venta> getVentas(){
		return (ArrayList<Venta>) this.persistenciaVenta.getVentas();
	}
	public ArrayList<Pago> getPagosContrato(Contrato contrato){
		return (ArrayList<Pago>) this.persistenciaCCcliente.getPagos_Contrato(contrato);
	}
	public ArrayList<ArancelEspecial> getArancelesContrato(Contrato contrato){
		return (ArrayList<ArancelEspecial>) this.persistenciaCCcliente.getAranceles_Contrato(contrato);
	}
	public String getEstadoDelContrato(Contrato contrato){
		return this.persistenciaCCcliente.getEstadoPagos_Contrato(contrato);
	}
	public ArrayList<Alquiler> getAlquileresPorDocumento(String documento){
		Cliente cliente = this.persistenciaCliente.getClientePorDocumento(documento);
		Locatario locatario = this.persistenciaCliente.getLocatarioPorCliente(cliente);
		
		return (ArrayList<Alquiler>) this.persistenciaAlquiler.getAlquilerPorLocatario(locatario);
	}
	public ArrayList<Venta> getVentasPorDocumento(String documento){
		Cliente cliente = this.persistenciaCliente.getClientePorDocumento(documento);
		Comprador comprador = this.persistenciaCliente.getCompradorPorCliente(cliente);
		
		return (ArrayList<Venta>) this.persistenciaVenta.getVentaPorComprador(comprador);
	}
	
	public ArrayList<Venta> obtenerVentaporComprador(Comprador comprador){
		return (ArrayList<Venta>) this.persistenciaVenta.getVentaPorComprador(comprador);
	}
	public boolean comprobarExisteGarante(Garante garante) {
		return this.persistenciaAlquiler.garanteRegistradoEnContrato(garante);
	}
	public ArrayList<Inmueble> obtenerInmueblePorLocador(Locador locador){
		return (ArrayList<Inmueble>) this.persistenciaInmuebles.getInmueblePorLocador(locador);
	}
	public String sumarMeses(String fechaYHora, long meses) {
        DateTimeFormatter formateador = DateTimeFormatter.ofPattern("uuuu-MM-dd");
        LocalDate fechaLocal = LocalDate.parse(fechaYHora, formateador);
        fechaLocal = fechaLocal.plusMonths(meses);
        return fechaLocal.format(formateador);
    }
	public String sumarDias(String fechaYHora, long dias) {
        DateTimeFormatter formateador = DateTimeFormatter.ofPattern("uuuu-MM-dd");
        LocalDate fechaLocal = LocalDate.parse(fechaYHora, formateador);
        fechaLocal = fechaLocal.plusDays(dias);
        return fechaLocal.format(formateador);
    }
	public List<Pago> getPagosCodContrato(String contrato){
		return  this.persistenciaCCcliente.getPagos_CodContrato(contrato);
	}
}		

