package controlador;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.webDao;
import modelo.Cliente;

/**
 * @email Ramesh Fadatare
 */


public class ControladorLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private webDao loginDao;


	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendRedirect("login.jsp");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			authenticate(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void authenticate(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		this.loginDao = new webDao();
		String username = request.getParameter("usuario-web");
		String password = request.getParameter("contrasenia");
		Cliente cliente=null;
		if ((cliente = this.loginDao.validar(username, password))!=null) {
			request.getSession().setAttribute("session", "ok");
			request.getSession().setAttribute("cliente", cliente);
			response.sendRedirect("cuentaCorriente.jsp");
		}else {
			request.getSession().setAttribute("session", "fail");
			response.sendRedirect("index.jsp");
		}
	}
	

}
